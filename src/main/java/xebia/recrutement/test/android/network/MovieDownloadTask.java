package xebia.recrutement.test.android.network;


import android.os.Handler;
import android.os.Looper;
import de.greenrobot.event.EventBus;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import xebia.recrutement.test.android.db.LocalStorage;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.domain.MovieComparator;
import xebia.recrutement.test.android.parser.MovieParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovieDownloadTask extends Thread {


    private String url;
    private LocalStorage localStorage;
    private long idParent = -1;

    public MovieDownloadTask(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        List<Movie> locaMovies = new ArrayList<Movie>();
        try {
            locaMovies = localStorage.getMovie();
            if (!locaMovies.isEmpty()) {
                Collections.sort(locaMovies, new MovieComparator());
                postInMainThread(locaMovies);
            }
            String movieData = launchRequest(url);
            List<Movie> movies = new MovieParser().parse(movieData);
            if (movies != null && !movies.isEmpty()) {
                Collections.sort(movies, new MovieComparator());
                postInMainThread(movies);
            } else {
                if (locaMovies.isEmpty()) {
                    postInMainThread(movies);
                }
            }
            if (idParent != -1) {
                localStorage.saveRelate(idParent, movies);
            } else {
                localStorage.saveMovie(movies);
            }

        } catch (Exception e) {
            postInMainThread(e);
        }

    }

    public void setIdParent(long idParent) {
        this.idParent = idParent;
    }

    public void setLocalStorage(LocalStorage localStorage) {
        this.localStorage = localStorage;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void postInMainThread(final Object object) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(object);
            }
        });
    }

    public String launchRequest(String url) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = httpclient.execute(httpGet);
        return EntityUtils.toString(response.getEntity());
    }
}
