package xebia.recrutement.test.android.utils;

import android.content.Context;
import android.content.Intent;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.activity.MovieDetailActivity;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.fragment.MovieInfoFragment;

/**
 * Created by lamine on 24/07/14.
 */
public class Util {


    public static void launchMovieDetailActivity(Context context, Movie movie) {
        Intent intent = new Intent(context, MovieDetailActivity.class);
        intent.putExtra(MovieInfoFragment.KEY_MOVIE, movie);
        context.startActivity(intent);
    }


    public void shareURL(Context context, String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.send_to)));
    }

}
