package xebia.recrutement.test.android.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;
import de.greenrobot.event.EventBus;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.db.LocalStorage;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.fragment.ListMovieFragment;
import xebia.recrutement.test.android.network.MovieDownloadTask;
import xebia.recrutement.test.android.utils.Util;
import xebia.recrutement.test.android.view.WaitingView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Home extends FragmentActivity {

    private static final String URL = "http://xebiamobiletest.herokuapp.com/api/public/v1.0/lists/movies/box_office.json";
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private ListMovieFragment listMovieFragment;
    private WaitingView waitingView;
    private boolean refresh;
    private List<Movie> movies = new ArrayList<Movie>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        listMovieFragment = (ListMovieFragment) getSupportFragmentManager().findFragmentById(R.id.list_fragment);
        waitingView = (WaitingView) findViewById(R.id.waitinn_view);
        EventBus.getDefault().register(this);
        if (savedInstanceState != null) {
            movies = savedInstanceState.getParcelableArrayList("list_movie");
        }
        if (movies == null || movies.isEmpty()) {
            loadData();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(listMovieFragment);
        if (!this.movies.isEmpty()) {
            EventBus.getDefault().post(this.movies);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(listMovieFragment);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        executorService.shutdownNow();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(ArrayList<Movie> movies) {
        this.movies = movies;
        if (movies == null || movies.isEmpty()) {
            waitingView.setMessage(R.string.no_data_found, true);
            waitingView.setVisibility(View.VISIBLE);
        } else {
            waitingView.setVisibility(View.GONE);
            EventBus.getDefault().post(this.movies);
        }
        if (refresh) {
            Toast.makeText(this, R.string.refresh_finish, Toast.LENGTH_SHORT).show();
            refresh = false;
        }
    }

    public void onEvent(Exception e) {
        waitingView.setMessage(R.string.error_loading, true);
        waitingView.setVisibility(View.VISIBLE);
    }


    public void onEvent(Movie movie) {
        Util.launchMovieDetailActivity(this, movie);
    }

    public void loadData() {
        MovieDownloadTask downloadTask = new MovieDownloadTask(URL);
        downloadTask.setLocalStorage(LocalStorage.getInstance(this));
        executorService = Executors.newSingleThreadExecutor();
        executorService.submit(downloadTask);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("list_movie", new ArrayList<Movie>(movies));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s != null && s.length() >= 2) {
                    EventBus.getDefault().post(findMovieMatching(s));
                } else {

                    EventBus.getDefault().post(Home.this.movies);
                }
                return false;
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh && !refresh) {
            Toast.makeText(this, R.string.refresh_start, Toast.LENGTH_SHORT).show();
            loadData();
            refresh = true;
            return true;
        }
        if (item.getItemId() == R.id.menu_watch_later) {
            startActivity(new Intent(this, WatchLater.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Movie> findMovieMatching(String text) {
        List<Movie> searchResult = new ArrayList<Movie>();
        for (Movie movie : this.movies) {
            if (movie.getSearchKeyword().contains(text)) {
                searchResult.add(movie);
            }
        }
        return searchResult;
    }


}

