package xebia.recrutement.test.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import de.greenrobot.event.EventBus;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.db.LocalStorage;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.fragment.MovieInfoFragment;
import xebia.recrutement.test.android.network.MovieDownloadTask;
import xebia.recrutement.test.android.utils.Util;
import xebia.recrutement.test.android.view.MovieSimilaryItem;
import xebia.recrutement.test.android.view.WaitingView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MovieDetailActivity extends FragmentActivity {


    private MovieInfoFragment movieInfoFragment;
    private WaitingView waitingView;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Movie movie;
    private ArrayList<Movie> similaryMovies = new ArrayList<Movie>();
    private LinearLayout linearLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initContent(savedInstanceState);

    }

    private void initContent(Bundle savedInstanceState) {
        setContentView(R.layout.layout_movie_detail_activity);
        movieInfoFragment = (MovieInfoFragment) getSupportFragmentManager().findFragmentById(R.id.movie_datail_containter);
        waitingView = (WaitingView) findViewById(R.id.waitinn_view);
        linearLayout = (LinearLayout) findViewById(R.id.list_similary);
        if (getMovieInfoFragment() == null) {
            movieInfoFragment = new MovieInfoFragment();
            FragmentTransaction fragmentTransaction = getFragmentTransaction();
            fragmentTransaction.add(R.id.movie_datail_containter, movieInfoFragment);
            movieInfoFragment.setArguments(getIntent().getExtras());
            fragmentTransaction.commitAllowingStateLoss();
        }
        movie = getIntent().getExtras().getParcelable(MovieInfoFragment.KEY_MOVIE);
        if (savedInstanceState != null) {
            similaryMovies = savedInstanceState.getParcelableArrayList("similar_movie");
        }
        if (similaryMovies == null || similaryMovies.isEmpty()) {
            similaryMovies = new ArrayList<Movie>();
            loadData(movie.getSimilarUrl());
        } else {
            onEvent(similaryMovies);
        }
    }

    FragmentTransaction getFragmentTransaction() {
        return getSupportFragmentManager().beginTransaction();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("similar_movie", similaryMovies);
    }


    public void onEvent(Movie movie) {
        //FIXME apply a good android practice here by adding launchmode
        finish();
        Util.launchMovieDetailActivity(this, movie);
    }

    public void loadData(String url) {
        MovieDownloadTask downloadTask = new MovieDownloadTask(url);
        downloadTask.setLocalStorage(LocalStorage.getInstance(this));
        downloadTask.setIdParent(movie.getId());
        executorService = Executors.newSingleThreadExecutor();
        executorService.submit(downloadTask);
    }

    public MovieInfoFragment getMovieInfoFragment() {
        return movieInfoFragment;
    }


    public void onEvent(ArrayList<Movie> movies) {
        if ((movies == null || movies.isEmpty() && (similaryMovies == null || similaryMovies.isEmpty()))) {
            waitingView.setMessage(R.string.error_loading, true);
            waitingView.setVisibility(View.VISIBLE);
        } else {
            waitingView.setVisibility(View.GONE);
            similaryMovies = movies;
            addSimilaryItem(similaryMovies);
        }
    }


    public void onEvent(Exception e) {
        waitingView.setMessage(R.string.error_loading_similary_movie, true);
        waitingView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_detail, menu);
        int resId = LocalStorage.getInstance(this).isWatchLater(this.movie.getId()) ? android.R.drawable.star_big_off : android.R.drawable.star_big_on;
        menu.findItem(R.id.menu_add_watch_later).setIcon(resId);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, this.movie.getSimilarUrl());
            shareIntent.setType("text/plain");
            startActivity(shareIntent);
            return true;
        }
        if (item.getItemId() == R.id.menu_add_watch_later) {
            LocalStorage.getInstance(this).updatWatchLater(movie.getId());
            int resId = LocalStorage.getInstance(this).isWatchLater(this.movie.getId()) ? android.R.drawable.star_big_off : android.R.drawable.star_big_on;
            item.setIcon(resId);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addSimilaryItem(List<Movie> movieList) {
        int size = movieList.size();
        linearLayout.removeAllViews();
        for (int i = 0; i < size && i < 3; i++) {

            linearLayout.addView(new MovieSimilaryItem(this, movieList.get(i)));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initContent(null);
    }
}
