package xebia.recrutement.test.android.activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.db.LocalStorage;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.fragment.ListMovieFragment;
import xebia.recrutement.test.android.view.WaitingView;

import java.util.ArrayList;
import java.util.List;


public class WatchLater extends FragmentActivity {


    ListMovieFragment listMovieFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_watch_later);
        listMovieFragment = (ListMovieFragment) getSupportFragmentManager().findFragmentById(R.id.list_fragment_container);
        WaitingView waitingView = (WaitingView) findViewById(R.id.waiting_view);
        List<Movie> movie = LocalStorage.getInstance(this).getWatchLater();
        if (movie.isEmpty()) {
            waitingView.setMessage(R.string.watch_later_empty, true);
        } else {
            addListMovieFragment(movie);
            waitingView.setVisibility(View.GONE);
        }

    }

    private void addListMovieFragment(List<Movie> movie) {
        if (listMovieFragment == null) {
            listMovieFragment = new ListMovieFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.list_fragment_container, listMovieFragment);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(ListMovieFragment.KEY_MOVIE_LIST, new ArrayList<Parcelable>(movie));
            listMovieFragment.setArguments(bundle);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }
}
