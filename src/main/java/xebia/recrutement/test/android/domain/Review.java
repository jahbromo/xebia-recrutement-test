package xebia.recrutement.test.android.domain;


public class Review {

    private long idMovie;
    private float note;

    public Review(long idMovie, float note) {
        this.idMovie = idMovie;
        this.note = note;

    }

    public long getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(long idMovie) {
        this.idMovie = idMovie;
    }

    public float getNote() {
        return note;
    }

    public void setNote(float note) {
        this.note = note;
    }
}


