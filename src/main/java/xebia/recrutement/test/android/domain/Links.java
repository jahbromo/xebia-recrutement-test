package xebia.recrutement.test.android.domain;


import android.os.Parcel;
import android.os.Parcelable;

public class Links implements Parcelable {
    // Parcelable interface
    public static final Parcelable.Creator<Links> CREATOR = new Parcelable.Creator<Links>() {

        @Override
        public Links[] newArray(
                int size) {
            return new Links[size];
        }

        @Override
        public Links createFromParcel(
                Parcel source) {
            return new Links(source);
        }
    };
    private String self;
    private String alternate;
    private String cast;
    private String clips;
    private String reviews;
    private String similar;

    Links(String self, String alternate, String cast, String clips, String reviews, String similar) {
        this.self = self;
        this.alternate = alternate;
        this.cast = cast;
        this.clips = clips;
        this.reviews = reviews;
        this.similar = similar;
    }

    Links(Parcel out) {
        this.self = out.readString();
        this.alternate = out.readString();
        this.cast = out.readString();
        this.clips = out.readString();
        this.reviews = out.readString();
        this.similar = out.readString();


    }

    public static Creator<Links> getCreator() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(self);
        parcel.writeString(alternate);
        parcel.writeString(cast);
        parcel.writeString(clips);
        parcel.writeString(reviews);
        parcel.writeString(similar);


    }

    public String getSimilar() {
        return similar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Links links = (Links) o;

        if (alternate != null ? !alternate.equals(links.alternate) : links.alternate != null) return false;
        if (cast != null ? !cast.equals(links.cast) : links.cast != null) return false;
        if (clips != null ? !clips.equals(links.clips) : links.clips != null) return false;
        if (reviews != null ? !reviews.equals(links.reviews) : links.reviews != null) return false;
        if (self != null ? !self.equals(links.self) : links.self != null) return false;
        if (similar != null ? !similar.equals(links.similar) : links.similar != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = self != null ? self.hashCode() : 0;
        result = 31 * result + (alternate != null ? alternate.hashCode() : 0);
        result = 31 * result + (cast != null ? cast.hashCode() : 0);
        result = 31 * result + (clips != null ? clips.hashCode() : 0);
        result = 31 * result + (reviews != null ? reviews.hashCode() : 0);
        result = 31 * result + (similar != null ? similar.hashCode() : 0);
        return result;
    }

}
