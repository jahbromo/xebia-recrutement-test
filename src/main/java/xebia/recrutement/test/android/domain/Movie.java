package xebia.recrutement.test.android.domain;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Movie implements Parcelable {
    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {

        @Override
        public Movie[] newArray(
                int size) {
            return new Movie[size];
        }

        @Override
        public Movie createFromParcel(
                Parcel source) {
            return new Movie(source);
        }
    };
    private long id;
    private String title;
    private short year;
    @SerializedName("mpaa_rating")
    private String mpaaRating;
    private int runtime;
    @SerializedName("critics_consensus")
    private String criticsConsensus;
    private String synopsis;
    @SerializedName("release_dates")
    private ReleaseDate releaseDate;
    @SerializedName("ratings")
    private Rating rating;
    private Posters posters;
    @SerializedName("abridged_cast")
    private List<AbridgedCast> abridgedCast;
    @SerializedName("alternate_ids")
    private AlternateIds alternateIds;
    private Links links;
    private String content;


    public Movie(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.year = (short) in.readInt();
        this.mpaaRating = in.readString();
        this.runtime = in.readInt();
        this.criticsConsensus = in.readString();
        this.synopsis = in.readString();
        this.releaseDate = in.readParcelable(ReleaseDate.class.getClassLoader());
        this.rating = in.readParcelable(Rating.class.getClassLoader());
        this.posters = in.readParcelable(Posters.class.getClassLoader());
        this.alternateIds = in.readParcelable(AlternateIds.class.getClassLoader());
        this.links = in.readParcelable(Links.class.getClassLoader());
        abridgedCast = new ArrayList<AbridgedCast>();
        in.readTypedList(abridgedCast, AbridgedCast.CREATOR);
        this.content = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeInt(year);
        dest.writeString(mpaaRating);
        dest.writeInt(runtime);
        dest.writeString(criticsConsensus);
        dest.writeString(synopsis);
        dest.writeParcelable(releaseDate, flags);
        dest.writeParcelable(rating, flags);
        dest.writeParcelable(posters, flags);
        dest.writeParcelable(alternateIds, flags);
        dest.writeParcelable(links, flags);
        dest.writeTypedList(abridgedCast);
        dest.writeString(content);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getSearchKeyword() {
        StringBuilder builder = new StringBuilder();
        builder.append(title);
        builder.append(year);
        builder.append(criticsConsensus);
        builder.append(synopsis);
        builder.append(releaseDate);
        builder.append(abridgedCast);
        return builder.toString();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (id != movie.id) return false;
        if (runtime != movie.runtime) return false;
        if (year != movie.year) return false;
        if (abridgedCast != null ? !abridgedCast.equals(movie.abridgedCast) : movie.abridgedCast != null) return false;
        if (alternateIds != null ? !alternateIds.equals(movie.alternateIds) : movie.alternateIds != null) return false;
        if (criticsConsensus != null ? !criticsConsensus.equals(movie.criticsConsensus) : movie.criticsConsensus != null)
            return false;
        if (links != null ? !links.equals(movie.links) : movie.links != null) return false;
        if (mpaaRating != null ? !mpaaRating.equals(movie.mpaaRating) : movie.mpaaRating != null) return false;
        if (posters != null ? !posters.equals(movie.posters) : movie.posters != null) return false;
        if (rating != null ? !rating.equals(movie.rating) : movie.rating != null) return false;
        if (releaseDate != null ? !releaseDate.equals(movie.releaseDate) : movie.releaseDate != null) return false;
        if (synopsis != null ? !synopsis.equals(movie.synopsis) : movie.synopsis != null) return false;
        if (title != null ? !title.equals(movie.title) : movie.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (int) year;
        result = 31 * result + (mpaaRating != null ? mpaaRating.hashCode() : 0);
        result = 31 * result + runtime;
        result = 31 * result + (criticsConsensus != null ? criticsConsensus.hashCode() : 0);
        result = 31 * result + (synopsis != null ? synopsis.hashCode() : 0);
        result = 31 * result + (releaseDate != null ? releaseDate.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (posters != null ? posters.hashCode() : 0);
        result = 31 * result + (abridgedCast != null ? abridgedCast.hashCode() : 0);
        result = 31 * result + (alternateIds != null ? alternateIds.hashCode() : 0);
        result = 31 * result + (links != null ? links.hashCode() : 0);
        return result;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnailUrl() {
        if (posters == null) {
            return null;
        }
        return this.posters.getThumbnail();
    }

    public String getProfileImageUrl() {
        if (posters == null) {
            return null;
        }
        return this.posters.getDetailed();
    }

    public int getRuntime() {
        return runtime;
    }

    public short getYear() {
        return year;
    }

    public String getCriticsConsensus() {
        return criticsConsensus;
    }

    public Rating getRating() {
        return this.rating;
    }

    public ReleaseDate getReleaseDate() {
        return releaseDate;
    }


    public String getSynopsis() {
        return synopsis;
    }

    public String getSimilarUrl() {
        if (this.links == null) {
            return null;
        }
        return this.links.getSimilar();
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
