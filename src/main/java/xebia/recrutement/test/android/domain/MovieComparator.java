package xebia.recrutement.test.android.domain;


import java.util.Comparator;

public class MovieComparator implements Comparator<Movie> {

    @Override
    public int compare(Movie lhs, Movie rhs) {
        return -lhs.getReleaseDate().getTheater().compareTo(rhs.getReleaseDate().getTheater());
    }
}
