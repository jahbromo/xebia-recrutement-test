package xebia.recrutement.test.android.domain;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

public class ReleaseDate implements Parcelable {

    // Parcelable interface
    public static final Parcelable.Creator<ReleaseDate> CREATOR = new Parcelable.Creator<ReleaseDate>() {

        @Override
        public ReleaseDate[] newArray(
                int size) {
            return new ReleaseDate[size];
        }

        @Override
        public ReleaseDate createFromParcel(
                Parcel source) {
            return new ReleaseDate(source);
        }
    };
    private Date theater;
    private Date dvd;

    ReleaseDate(Date theater, Date dvd) {
        this.theater = theater;
        this.dvd = dvd;
    }

    ReleaseDate(Parcel out) {

        long dvdTime = out.readLong();
        if (dvdTime != -1) {
            dvd = Calendar.getInstance().getTime();
            dvd.setTime(dvdTime);
        }
        long threaterTime = out.readLong();
        if (threaterTime != -1) {
            theater = Calendar.getInstance().getTime();
            theater.setTime(threaterTime);
        }

    }

    @Override
    public String toString() {
        if (dvd == null && theater == null) {
            return "";
        }
        if (dvd == null) {
            return theater.toString();
        }
        if (theater == null) {
            return dvd.toString();
        }
        return this.dvd.toString() + theater.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(dvd == null ? -1 : dvd.getTime());
        parcel.writeLong(theater == null ? -1 : theater.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReleaseDate that = (ReleaseDate) o;

        if (dvd != null ? !dvd.equals(that.dvd) : that.dvd != null) return false;
        if (theater != null ? !theater.equals(that.theater) : that.theater != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = theater != null ? theater.hashCode() : 0;
        result = 31 * result + (dvd != null ? dvd.hashCode() : 0);
        return result;
    }

    public Date getTheater() {
        return theater;
    }
}
