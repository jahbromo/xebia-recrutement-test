package xebia.recrutement.test.android.domain;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Rating implements Parcelable {

    // Parcelable interface
    public static final Parcelable.Creator<Rating> CREATOR = new Creator<Rating>() {

        @Override
        public Rating[] newArray(
                int size) {
            return new Rating[size];
        }

        @Override
        public Rating createFromParcel(
                Parcel source) {
            return new Rating(source);
        }
    };
    @SerializedName("critics_rating")
    private String criticsRating;
    @SerializedName("critics_score")
    private int criticsScore;
    @SerializedName("audience_rating")
    private String audienceRating;
    @SerializedName("audience_score")
    private int audienceScore;

    Rating(String criticsRating, int criticsScore, String audienceRating, int audienceScore) {
        this.criticsRating = criticsRating;
        this.audienceScore = audienceScore;
        this.audienceRating = audienceRating;
        this.criticsScore = criticsScore;
    }

    Rating(Parcel in) {
        this(in.readString(), in.readInt(), in.readString(), in.readInt());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(criticsRating);
        parcel.writeInt(criticsScore);
        parcel.writeString(audienceRating);
        parcel.writeInt(audienceScore);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating rating = (Rating) o;

        if (audienceScore != rating.audienceScore) return false;
        if (criticsScore != rating.criticsScore) return false;
        if (audienceRating != null ? !audienceRating.equals(rating.audienceRating) : rating.audienceRating != null)
            return false;
        if (criticsRating != null ? !criticsRating.equals(rating.criticsRating) : rating.criticsRating != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = criticsRating != null ? criticsRating.hashCode() : 0;
        result = 31 * result + criticsScore;
        result = 31 * result + (audienceRating != null ? audienceRating.hashCode() : 0);
        result = 31 * result + audienceScore;
        return result;
    }

    public String getCriticsRating() {
        return criticsRating;
    }

    public int getCriticsScore() {
        return criticsScore;
    }

    public int getAudienceScore() {
        return audienceScore;
    }
}
