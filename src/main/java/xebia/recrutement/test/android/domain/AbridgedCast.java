package xebia.recrutement.test.android.domain;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class AbridgedCast implements Parcelable {
    // Parcelable interface
    public static final Parcelable.Creator<AbridgedCast> CREATOR = new Creator<AbridgedCast>() {

        @Override
        public AbridgedCast[] newArray(
                int size) {
            return new AbridgedCast[size];
        }

        @Override
        public AbridgedCast createFromParcel(
                Parcel source) {
            return new AbridgedCast(source);
        }
    };
    private String name;
    private int id;
    private List<String> characters;

    AbridgedCast(String name, int id, List<String> characters) {
        this.name = name;
        this.id = id;
        this.characters = characters;
    }

    AbridgedCast(Parcel out) {
        this.id = out.readInt();
        this.name = out.readString();
        this.characters = new ArrayList<String>();
        out.readStringList(characters);
    }

    @Override
    public String toString() {
        if (this.name == null && characters == null) {
            return "";
        }
        if (this.name == null) {
            return characters.toString();
        }
        if (characters == null) {
            return name;
        }
        return this.name + characters.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeStringList(characters);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbridgedCast that = (AbridgedCast) o;

        if (id != that.id) return false;
        if (characters != null ? !characters.equals(that.characters) : that.characters != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + (characters != null ? characters.hashCode() : 0);
        return result;
    }
}
