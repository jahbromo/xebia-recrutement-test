package xebia.recrutement.test.android.domain;


import android.os.Parcel;
import android.os.Parcelable;

public class AlternateIds implements Parcelable {
    // Parcelable interface
    public static final Parcelable.Creator<AlternateIds> CREATOR = new Creator<AlternateIds>() {

        @Override
        public AlternateIds[] newArray(
                int size) {
            return new AlternateIds[size];
        }

        @Override
        public AlternateIds createFromParcel(
                Parcel source) {
            return new AlternateIds(source);
        }
    };
    private long imdb;

    AlternateIds(long imdb) {
        this.imdb = imdb;
    }


    AlternateIds(Parcel out) {
        this(out.readLong());

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(imdb);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlternateIds that = (AlternateIds) o;

        if (imdb != that.imdb) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (imdb ^ (imdb >>> 32));
    }
}
