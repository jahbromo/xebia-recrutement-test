package xebia.recrutement.test.android.domain;


import android.os.Parcel;
import android.os.Parcelable;

public class Posters implements Parcelable {
    // Parcelable interface
    public static final Parcelable.Creator<Posters> CREATOR = new Parcelable.Creator<Posters>() {

        @Override
        public Posters[] newArray(
                int size) {
            return new Posters[size];
        }

        @Override
        public Posters createFromParcel(
                Parcel source) {
            return new Posters(source);
        }
    };
    private String thumbnail;
    private String profile;
    private String detailed;
    private String original;

    public Posters(String thumbnail, String profile, String detailed, String original) {
        this.thumbnail = thumbnail;
        this.profile = profile;
        this.detailed = detailed;
        this.original = original;
    }

    Posters(Parcel out) {
        this(out.readString(), out.readString(), out.readString(), out.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(thumbnail);
        parcel.writeString(profile);
        parcel.writeString(detailed);
        parcel.writeString(original);


    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getDetailed() {
        return detailed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Posters posters = (Posters) o;

        if (detailed != null ? !detailed.equals(posters.detailed) : posters.detailed != null) return false;
        if (original != null ? !original.equals(posters.original) : posters.original != null) return false;
        if (profile != null ? !profile.equals(posters.profile) : posters.profile != null) return false;
        if (thumbnail != null ? !thumbnail.equals(posters.thumbnail) : posters.thumbnail != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = thumbnail != null ? thumbnail.hashCode() : 0;
        result = 31 * result + (profile != null ? profile.hashCode() : 0);
        result = 31 * result + (detailed != null ? detailed.hashCode() : 0);
        result = 31 * result + (original != null ? original.hashCode() : 0);
        return result;
    }
}
