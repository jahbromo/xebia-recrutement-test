package xebia.recrutement.test.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.domain.Review;
import xebia.recrutement.test.android.parser.MovieParser;

import java.util.ArrayList;
import java.util.List;


public class LocalStorage extends SQLiteOpenHelper {

    private static final String databaseName = "dbboxoffice";
    private static final int databaseVersion = 1;
    private static LocalStorage instance;
    private SQLiteDatabase db;

    private LocalStorage(Context context) {
        super(context, databaseName, null, databaseVersion);
        db = getWritableDatabase();

    }

    public final static LocalStorage getInstance(Context context) {
        if (instance == null) {
            synchronized (LocalStorage.class) {
                if (instance == null) {
                    instance = new LocalStorage(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS movie (idmovie INTEGER UNIQUE, content TEXT)");
        db.execSQL("CREATE TABLE IF NOT EXISTS relate (idmovie INTEGER , idparent INTEGER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS watch (idmovie INTEGER UNIQUE , added_time INTEGER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS review (idmovie INTEGER UNIQUE , note REAL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS  movie");
        onCreate(db);
    }

    public void saveMovie(List<Movie> movies) {
        try {
            String sql = "insert or replace into movie (idmovie, content) values(?,?)";
            db.beginTransaction();
            SQLiteStatement insertStatement = db.compileStatement(sql);
            for (Movie movie : movies) {
                insertStatement.bindLong(1, movie.getId());
                insertStatement.bindString(2, movie.getContent());
                insertStatement.execute();
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }


    }


    public List<Movie> getMovie() {
        Cursor cursor = db.query("movie", new String[]{"content"}, null, null, null, null, null);
        return retrieveMovie(cursor);
    }

    private List<Movie> retrieveMovie(Cursor cursor) {
        List<Movie> movies = new ArrayList<Movie>();
        while (cursor != null && cursor.moveToNext()) {
            Movie movie = new MovieParser().parseMovie(cursor.getString(0));
            if (movie != null) {
                movies.add(movie);
            }
        }
        return movies;
    }

    public void saveRelate(long idParent, List<Movie> movies) {
        saveMovie(movies);

        try {
            String sql = "insert or replace into relate (idmovie, idparent) values(?,?)";
            db.beginTransaction();
            SQLiteStatement insertStatement = db.compileStatement(sql);
            insertStatement.bindLong(1, idParent);
            for (Movie movie : movies) {
                insertStatement.bindLong(2, movie.getId());
                insertStatement.execute();
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }

    }

    public List<Movie> getRelate(long idParent) {
        String selectQuery = "select content from movie tm , relate tr where tm.idmovie==tr.idmovie and tr.idparent=" + idParent;
        Cursor c = db.rawQuery(selectQuery, null);
        return retrieveMovie(c);
    }

    public void saveReview(Review review) {
        db.delete("review", "idmovie= ?", new String[]{String.valueOf(review.getIdMovie())});
        ContentValues args = new ContentValues();
        args.put("idmovie", review.getIdMovie());
        args.put("note", review.getNote());
        db.insert("review", null, args);

    }

    public Review getReview(long idMovie) {
        Cursor cursor = db.query("review", null, "idmovie  = ?", new String[]{String.valueOf(idMovie)}, null, null, null);
        if (cursor != null && cursor.moveToNext()) {
            return new Review(idMovie, cursor.getFloat(1));
        }
        return null;

    }


    public List<Movie> getWatchLater() {
        String selectQuery = "select content from movie tm , watch tw where tm.idmovie==tw.idmovie order by tw.added_time DESC";
        Cursor c = db.rawQuery(selectQuery, null);
        return retrieveMovie(c);
    }


    public boolean isWatchLater(long idMovie) {
        Cursor cursor = db.query("watch", new String[]{"idmovie"}, "idmovie  = ?", new String[]{String.valueOf(idMovie)}, null, null, null);
        if (cursor != null && cursor.moveToNext()) {
            return true;
        }
        return false;
    }

    public void updatWatchLater(long idMovie) {
        if (!isWatchLater(idMovie)) {
            ContentValues args = new ContentValues();
            args.put("idmovie", idMovie);
            args.put("added_time", System.currentTimeMillis());
            db.insert("watch", null, args);
        } else {
            db.delete("watch", "idmovie= ?", new String[]{String.valueOf(idMovie)});
        }
    }


}
