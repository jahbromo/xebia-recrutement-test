package xebia.recrutement.test.android.parser;


import com.google.gson.*;
import xebia.recrutement.test.android.domain.Movie;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MovieParser {

    private Gson gson;

    public MovieParser() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        gson = gsonBuilder.create();
    }

    public List<Movie> parse(String data) {
        List<Movie> result = new ArrayList<Movie>();
        try {
            JsonArray movies = new JsonParser().parse(data).getAsJsonObject().getAsJsonArray("movies");
            for (int i = 0; i < movies.size(); i++) {
                JsonElement jsonElement = movies.get(i);
                result.add(parseMovie(jsonElement.toString()));
            }
        } catch (Exception e) {
        }
        if (result == null) {
            result = new ArrayList<Movie>();
        }
        return result;
    }

    public Movie parseMovie(String content) {
        Movie movie = gson.fromJson(content, Movie.class);
        movie.setContent(content);
        return movie;
    }
}
