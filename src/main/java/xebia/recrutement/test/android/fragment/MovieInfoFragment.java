package xebia.recrutement.test.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import de.greenrobot.event.EventBus;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.db.LocalStorage;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.domain.Review;
import xebia.recrutement.test.android.view.MovieHeaderInfo;
import xebia.recrutement.test.android.view.MovieUserReview;


public class MovieInfoFragment extends Fragment {

    public static final String KEY_MOVIE = "movie";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_movie_detail, container, false);
        Movie movie = getArguments().getParcelable(KEY_MOVIE);
        ((MovieHeaderInfo) view.findViewById(R.id.movie_header_info)).set(movie);
        Review review = LocalStorage.getInstance(getActivity()).getReview(movie.getId());
        if (review == null) {
            review = new Review(movie.getId(), 0);
        }
        ((MovieUserReview) view.findViewById(R.id.movie_user_review)).setMovie(review);
        ((TextView) view.findViewById(R.id.movie_critic)).setText(movie.getCriticsConsensus());
        ((TextView) view.findViewById(R.id.movie_synopsie)).setText(movie.getSynopsis());
        ((TextView) view.findViewById(R.id.movie_info_title)).setText(movie.getTitle());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(Review review) {
        LocalStorage.getInstance(getActivity()).saveReview(review);
    }
}
