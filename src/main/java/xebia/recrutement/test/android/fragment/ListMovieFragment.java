package xebia.recrutement.test.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import de.greenrobot.event.EventBus;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.domain.Movie;
import xebia.recrutement.test.android.view.MovieListAdapter;

import java.util.ArrayList;


public class ListMovieFragment extends Fragment {

    public static String KEY_MOVIE_LIST = "movies";
    private MovieListAdapter movieListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.movie_list_fragment, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        Bundle bundle = getArguments();
        movieListAdapter = new MovieListAdapter();
        ArrayList<Movie> movies = null;
        if (bundle != null) {
            movies = bundle.getParcelableArrayList(KEY_MOVIE_LIST);
        }
        if (movies != null) {
            movieListAdapter.setMovies(movies);
        }
        listView.setAdapter(movieListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventBus.getDefault().post(movieListAdapter.getItem(position));
            }
        });
        return view;
    }

    public void onEVent(ArrayList<Movie> movies) {
        movieListAdapter.setMovies(movies);
    }

}
