package xebia.recrutement.test.android.controller;

import xebia.recrutement.test.android.domain.Rating;

public class RatingController {

    private static int REF_RATING = 5;
    private Rating rating;
    private float RATE = 5.0f / 100.0f;

    public RatingController(Rating rating) {
        this.rating = rating;
    }

    public float getGblobalRating() {
        int mean = (this.rating.getAudienceScore() + this.rating.getCriticsScore()) / 2;
        return mean * RATE;
    }

    public float getCriticRating() {
        return this.rating.getCriticsScore() * RATE;
    }

    public float getAudienceRating() {
        return this.rating.getAudienceScore() * RATE;
    }
}
