package xebia.recrutement.test.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.controller.RatingController;
import xebia.recrutement.test.android.domain.Movie;


public class MovieItemView extends LinearLayout {

    private ImageView imageView;
    private TextView nameView;
    private TextView movieCritic;
    private TextView movieDateView;
    private TextView movieRuntime;
    private TextView rating;
    private RatingController ratingController;

    public MovieItemView(Context context, Movie movie) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.movie_item_view, this, true);
        nameView = (TextView) findViewById(R.id.movie_title);
        movieDateView = (TextView) findViewById(R.id.movie_release_date);
        movieCritic = (TextView) findViewById(R.id.movie_critic);
        movieRuntime = (TextView) findViewById(R.id.runtime);
        rating = (TextView) findViewById(R.id.movie_rating);
        this.imageView = (ImageView) findViewById(R.id.movie_thumbnail);
        this.ratingController = new RatingController(movie.getRating());
        this.reuse(movie);
    }

    public void reuse(Movie movie) {
        this.nameView.setText(String.valueOf(movie.getTitle()));
        this.movieDateView.setText(String.valueOf(movie.getYear()));
        this.movieCritic.setText(movie.getCriticsConsensus());
        long durationInSeconde = movie.getRuntime() * 60 * 1000;
        this.movieRuntime.setText(String.valueOf(String.format("%tR", durationInSeconde)));
        this.setTag(String.valueOf(movie.getSearchKeyword()));
        this.rating.setText(Math.round(ratingController.getGblobalRating()) + "/5");
        Picasso.with(getContext())
                .load(movie.getProfileImageUrl())
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .into(imageView);
    }
}
