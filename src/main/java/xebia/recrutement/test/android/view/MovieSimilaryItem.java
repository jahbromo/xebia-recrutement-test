package xebia.recrutement.test.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.squareup.picasso.Picasso;
import de.greenrobot.event.EventBus;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.domain.Movie;


public class MovieSimilaryItem extends LinearLayout {

    private ImageView imageView;


    public MovieSimilaryItem(Context context, final Movie movie) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.movie_similar_item_view, this, true);
        this.imageView = (ImageView) findViewById(R.id.movie_thumbnail);
        this.reuse(movie);
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                EventBus.getDefault().post(movie);
                return false;
            }
        });

    }

    private void reuse(Movie movie) {
        this.setTag(String.valueOf(movie.getSearchKeyword()));
        Picasso.with(getContext())
                .load(movie.getProfileImageUrl())
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .into(imageView);
    }
}
