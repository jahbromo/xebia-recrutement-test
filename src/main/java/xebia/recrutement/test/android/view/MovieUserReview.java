package xebia.recrutement.test.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import de.greenrobot.event.EventBus;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.domain.Review;


public class MovieUserReview extends LinearLayout implements RatingBar.OnRatingBarChangeListener {


    private RatingBar ratingBar;
    private Review review;

    public MovieUserReview(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_movie_user_review, this, true);
        ratingBar = (RatingBar) findViewById(R.id.use_rating);
        ratingBar.setOnRatingBarChangeListener(this);

    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
        if (review != null) {
            review.setNote(v);
        }
        EventBus.getDefault().post(review);
    }

    public void setMovie(Review review) {
        this.review = review;
        ratingBar.setRating(review.getNote());
    }
}
