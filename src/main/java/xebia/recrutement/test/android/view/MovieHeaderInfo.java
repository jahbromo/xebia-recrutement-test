package xebia.recrutement.test.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import xebia.recrutement.test.android.R;
import xebia.recrutement.test.android.controller.RatingController;
import xebia.recrutement.test.android.domain.Movie;

import java.util.Date;


public class MovieHeaderInfo extends LinearLayout {


    private TextView nameView;
    private ImageView imageView;
    private TextView releaseDate;
    private RatingBar criticRating;
    private RatingBar audienceRating;


    public MovieHeaderInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.movie_header_info, this, true);
        this.imageView = (ImageView) findViewById(R.id.movie_detailed_image);
        this.releaseDate = (TextView) findViewById(R.id.movie_release_date);
        this.audienceRating = (RatingBar) findViewById(R.id.movie_header_rating_audience);
        this.criticRating = (RatingBar) findViewById(R.id.movie_header_rating_critic);
    }

    public void set(Movie movie) {
        Picasso.with(getContext()).load(movie.getProfileImageUrl()).into(imageView);
        Date date = movie.getReleaseDate() == null ? null : movie.getReleaseDate().getTheater() == null ? null : movie.getReleaseDate().getTheater();
        if (date != null) {
            this.releaseDate.setText(getContext().getString(R.string.release_date) + String.format(" %td %tb %tY", date, date, date));
        } else {
            this.releaseDate.setText(getContext().getString(R.string.release_date) + "    --   ");
        }
        RatingController ratingController = new RatingController(movie.getRating());
        this.audienceRating.setRating(ratingController.getAudienceRating());
        this.criticRating.setRating(ratingController.getCriticRating());
    }
}
