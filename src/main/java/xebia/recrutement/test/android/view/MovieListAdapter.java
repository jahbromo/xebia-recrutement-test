package xebia.recrutement.test.android.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import xebia.recrutement.test.android.domain.Movie;

import java.util.ArrayList;
import java.util.List;


public class MovieListAdapter extends BaseAdapter {
    private List<Movie> movies;

    public MovieListAdapter() {
        this.movies = new ArrayList<Movie>();
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetInvalidated();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Movie movie = movies.get(position);
        return new MovieItemView(parent.getContext(), movie);

    }

}
