package xebia.recrutement.test.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import xebia.recrutement.test.android.R;


public class WaitingView extends LinearLayout {

    private TextView nameView;
    private View view;

    public WaitingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.waiting_view, this, true);
        nameView = (TextView) findViewById(R.id.loarding_message_view);
        this.view = findViewById(R.id.progressBar1);
    }

    public TextView getNameView() {
        return nameView;
    }

    public View getView() {
        return view;
    }

    public void setMessage(int idRessource, boolean isError) {
        this.nameView.setText(idRessource);
        this.view.setVisibility(isError ? View.GONE : View.VISIBLE);
    }


}
