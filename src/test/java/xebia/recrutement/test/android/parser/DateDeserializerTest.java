package xebia.recrutement.test.android.parser;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@RunWith(RobolectricTestRunner.class)
public class DateDeserializerTest {

    DateDeserializer dateDeserializer = new DateDeserializer();

    @Test
    public void shouldDeserializeStringInDate() {
        JsonElement result = new JsonPrimitive("2014-08-23");
        Date date = dateDeserializer.deserialize(result, null, null);
        Calendar calendar = Calendar.getInstance(Locale.FRANCE);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);
        Assert.assertEquals(calendar.get(Calendar.YEAR), 2014);
        Assert.assertEquals(calendar.get(Calendar.MONTH), 7);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 23);

    }

    @Test
    public void shouldReturnNullForEmptyInvalidDate() {
        Assert.assertNull(dateDeserializer.deserialize(new JsonPrimitive(""), null, null));
        Assert.assertNull(dateDeserializer.deserialize(new JsonPrimitive("bad_string_as_date"), null, null));
    }

}