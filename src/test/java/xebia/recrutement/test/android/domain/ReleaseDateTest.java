package xebia.recrutement.test.android.domain;

import android.os.Parcel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Calendar;
import java.util.Date;

@RunWith(RobolectricTestRunner.class)
public class ReleaseDateTest {

    @Test
    public void testParcel() {

        Date dvd = Calendar.getInstance().getTime();
        Date threater = Calendar.getInstance().getTime();
        ReleaseDate orignalRelease = new ReleaseDate(dvd, threater);
        Parcel lc_parcel = Parcel.obtain();
        orignalRelease.writeToParcel(lc_parcel, 0);
        lc_parcel.setDataPosition(0);

        ReleaseDate releaseDateAfterParcel = new ReleaseDate(lc_parcel);
        Assert.assertEquals(orignalRelease, releaseDateAfterParcel);
        Assert.assertEquals(orignalRelease.getTheater(), releaseDateAfterParcel.getTheater());
    }

}