package xebia.recrutement.test.android.domain;

import android.os.Parcel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;


@RunWith(RobolectricTestRunner.class)
public class AlternateIdsTest {


    @Test
    public void testParcel() {
        AlternateIds orignal = new AlternateIds(1212L);
        Parcel lc_parcel = Parcel.obtain();
        orignal.writeToParcel(lc_parcel, 0);
        lc_parcel.setDataPosition(0);

        AlternateIds result = new AlternateIds(lc_parcel);
        Assert.assertEquals(orignal, result);

    }
}