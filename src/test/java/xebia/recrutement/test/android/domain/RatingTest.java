package xebia.recrutement.test.android.domain;

import android.os.Parcel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class RatingTest {


    @Test
    public void testParcel() {
        Rating orignal = new Rating("self", 121, "cast", 344);
        Parcel lc_parcel = Parcel.obtain();
        orignal.writeToParcel(lc_parcel, 0);
        lc_parcel.setDataPosition(0);
        Rating result = new Rating(lc_parcel);
        Assert.assertEquals(orignal, result);
    }
}