package xebia.recrutement.test.android.domain;

import android.os.Parcel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class PostersTest {


    @Test
    public void testParcel() {
        Posters original = new Posters("self", "", null, "original");
        Parcel lc_parcel = Parcel.obtain();
        original.writeToParcel(lc_parcel, 0);
        lc_parcel.setDataPosition(0);
        Posters result = new Posters(lc_parcel);
        Assert.assertEquals(original, result);
    }
}
