package xebia.recrutement.test.android.domain;

import android.os.Parcel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;


@RunWith(RobolectricTestRunner.class)
public class LinksTest {


    @Test
    public void testParcel() {
        Links orignal = new Links("self", "alternate", "cast", null, null, "");
        Parcel lc_parcel = Parcel.obtain();
        orignal.writeToParcel(lc_parcel, 0);
        lc_parcel.setDataPosition(0);

        Links result = new Links(lc_parcel);
        Assert.assertEquals(orignal, result);
    }
}