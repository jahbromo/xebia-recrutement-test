package xebia.recrutement.test.android.domain;

import android.os.Parcel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import xebia.recrutement.test.android.util.TestUtil;


@RunWith(RobolectricTestRunner.class)
public class MovieTest {


    @Test
    public void shouldParcelMovie() {
        Movie movie = TestUtil.createMovie();

        Parcel lc_parcel = Parcel.obtain();
        movie.writeToParcel(lc_parcel, 0);
        lc_parcel.setDataPosition(0);
        Movie result = new Movie(lc_parcel);
        Assert.assertEquals(movie, result);
    }


}