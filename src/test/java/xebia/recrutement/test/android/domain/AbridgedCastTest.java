package xebia.recrutement.test.android.domain;

import android.os.Parcel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
public class AbridgedCastTest {


    @Test
    public void testParcel() {
        List<String> characters = new ArrayList<String>();
        characters.add("ch");
        AbridgedCast orignalAbridgedCast = new AbridgedCast("John Q", 1212, characters);
        Parcel lc_parcel = Parcel.obtain();
        orignalAbridgedCast.writeToParcel(lc_parcel, 0);
        lc_parcel.setDataPosition(0);

        AbridgedCast result = new AbridgedCast(lc_parcel);
        Assert.assertEquals(orignalAbridgedCast, result);
    }
}