//package xebia.recrutement.test.android.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.robolectric.Robolectric;
//import org.robolectric.RobolectricTestRunner;
//import org.robolectric.util.ActivityController;
//import xebia.recrutement.test.android.domain.Movie;
//import xebia.recrutement.test.android.util.TestUtil;
//import xebia.recrutement.test.android.fragment.MovieInfoFragment;
//
//import static org.junit.Assert.assertNotNull;
//
//@RunWith(RobolectricTestRunner.class)
//public class MovieDetailActivityTest {
//
//    @Before
//
//    @Test
//    public void checkActivityNotNull() throws Exception {
//       MovieDetailActivity activity = Robolectric.buildActivity(MovieDetailActivity.class).create().get();
//        assertNotNull(activity);
//    }
//
//    @Test
//    public void checkMovieFragmentShouldBeAdded() throws Exception {
//       MovieDetailActivity activity = Robolectric.buildActivity(MovieDetailActivity.class)
//                .create().get();
//        Assert.assertTrue(activity.getMovieInfoFragment().isAdded());
//    }
//
//    @Test
//    public void checkMovieDetailActivityShouldPasseItsExtraToFragment() throws Exception {
//
//        Intent intent = new Intent(Robolectric.application,
//                MovieDetailActivity.class);
//        Bundle extras = new Bundle();
//        Movie originalMovie= TestUtil.createMovie();
//        extras.putParcelable(MovieInfoFragment.KEY_MOVIE, originalMovie);
//        intent.putExtras(extras);
//        ActivityController controller=Robolectric.buildActivity(MovieDetailActivity.class);
//        MovieDetailActivity activity = (MovieDetailActivity)controller
//                .withIntent(intent)
//                .create()
//                .start()
//                .visible()
//                .get();
//        Assert.assertEquals(originalMovie, activity.getMovieInfoFragment().getArguments().getParcelable(MovieInfoFragment.KEY_MOVIE));
//    }
//}